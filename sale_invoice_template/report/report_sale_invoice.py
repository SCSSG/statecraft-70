from report import report_sxw
from datetime import date
from datetime import datetime
import time
import tools
import unicodedata

class sale_account_invoice(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(sale_account_invoice, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object'      : self._get_object,
								  'get_total'		: self._get_total,
								  'get_tax_info'	: self._get_tax_info,
								  'get_bank'		: self._get_bank,
								  'get_order'		: self._get_order,
								  'get_order_line'	: self._get_order_line,
								  'get_tax'			: self._get_tax,
								  'get_picking_info': self._get_picking_info,
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	def _get_picking_info(self,invoice):
		cr = self.cr
		uid = self.uid
		res = {
		'exterior_aspect': '-',
		'package':0
		}
		if invoice.origin:
			origin = invoice.origin.split(":")
			print 'origin',origin
			if len(origin)==2:
				source_delivery = str(origin[0])
				print source_delivery,type(source_delivery)
				delivery = self.pool.get('stock.picking').search(cr,uid,[('name','=',source_delivery)])
				print delivery
				if delivery:
					delivery_obj = self.pool.get('stock.picking.out').browse(cr,uid,delivery[0])
					res['exterior_aspect']=delivery_obj.exterior_aspect
					res['package']=delivery_obj.package
		return res


	def _get_tax(self,line):
		tax=''
		for tax_obj in line.invoice_line_tax_id:
			tax += ' ' + tax_obj.name
		return tax

	def _get_order(self,invoice):
		print invoice
		order=False
		order_ids = self.pool.get('sale.order').search(self.cr,self.uid,[('invoice_ids','=',invoice.id)])
		if order_ids:
			order = self.pool.get('sale.order').browse(self.cr,self.uid,order_ids[0])
		return order

	def _get_order_line(self,invoice_line):
		pos = ' '
		for order_line in invoice_line.sale_lines:
			pos += order_line.pos or ' ' + ' '
		return pos


	def _get_bank(self,partner):
		if partner.bank_ids:
			bank_obj=partner.bank_ids[0]
			bank={
			'name': bank_obj.bank_name,
			'number': bank_obj.acc_number,
			'swiftcode': bank_obj.bank_bic
			}
		else:
			bank={
			'name': ' ',
			'number': ' ',
			'swiftcode': ' '
			}
		return bank


	def _get_total(self,invoice_lines,tax_lines):
		res = {
		'total_qty' : 0.0,
		'total_net' : 0.0,
		'total_tax' : 0.0,
		'total_amt' : 0.0,

		'weight'	: 0.0,
		'weight_net': 0.0
		}
		for line in invoice_lines:
			weight = line.product_id and line.product_id.weight or 0.0
			weight_net = line.product_id and line.product_id.weight_net or 0.0

			if line.product_id.type != 'service':
				res['total_qty'] += line.quantity
			res['total_net'] += line.price_subtotal
			res['weight']	 += weight
			res['weight_net']+= weight_net

		for line in tax_lines:
			res['total_tax'] += line.amount

		res['total_amt'] = res['total_net'] + res['total_tax']
		return res

	def _get_tax_info(self,invoice_lines):
		res={
		'code'		: '',
		'percentage': '',
		'name'		: ''
		}
		for line in invoice_lines:
			for tax in line.invoice_line_tax_id:
				res['code']			= tax.description or ''
				res['percentage']	= str(int((tax.amount * 100)))
				res['name']			= tax.name
		return res



report_sxw.report_sxw('report.sale.account.invoice', 'account.invoice', 'sale_invoice_template/report/report_sale_invoice.mako', parser=sale_account_invoice)