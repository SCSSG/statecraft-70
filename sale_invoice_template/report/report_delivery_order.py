from report import report_sxw
from datetime import date
from datetime import datetime
import time
import tools
import unicodedata

class delivery_order(report_sxw.rml_parse):

	def __init__(self, cr, uid, name, context):
		super(delivery_order, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
					  'get_object'      : self._get_object,
					  'get_order'		: self._get_order,
					  'get_total'		: self._get_total
					  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	def _get_order(self,do):
		order=False
		order_ids = self.pool.get('sale.order').search(self.cr,self.uid,[('name','=',do.origin)])
		if order_ids:
			order = self.pool.get('sale.order').browse(self.cr,self.uid,order_ids[0])
		return order

	def _get_total(self,move_lines):
		res = {
		'total_qty' : 0.0,
		}
		for line in move_lines:
			if line.product_id.type != 'service':
				res['total_qty'] += line.product_qty
		return res
report_sxw.report_sxw('report.delivery.order', 'stock.picking.out', 'sale_invoice_template/report/report_delivery_order.mako', parser=delivery_order)