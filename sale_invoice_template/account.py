from osv import osv,fields

class account_invoice_line(osv.osv):
	_inherit = "account.invoice.line"
	_columns = {
		'sale_lines': fields.many2many('sale.order.line', 'sale_order_line_invoice_rel', 'invoice_id', 'order_line_id', 'Sale Order Lines', readonly=True),
	}

class account_invoice(osv.osv):
	_inherit = "account.invoice"
	_columns = {
    	'exterior_aspect': fields.char('Packing Exterior Aspect',size=200),
    	'package': fields.integer('Package'),
		'gross_weight': fields.float('Gross Weight (kg)'),
		'net_weight': fields.float('Net Weight (kg)'),
		'shipment_method': fields.char('Shipment Method',size=100),
		'incoterm': fields.many2one('stock.incoterms','Incoterms')
    }