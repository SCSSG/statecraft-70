from osv import osv,fields
from openerp import netsvc
import time
import datetime


class sale_order(osv.osv):
	_inherit = "sale.order"

	def _get_validity_date(self, cr, uid, context=None):

		now = datetime.datetime.now()
		days60 = datetime.timedelta(days=60)
		validity_date = now + days60
		return validity_date.strftime('%Y-%m-%d')

	_columns = {
		'shipment_method': fields.char('Shipment Method',size=100),
		'validity_date': fields.date('Validity Date')
	}

	_defaults = {
		'validity_date': _get_validity_date
	}

	def print_quotation(self, cr, uid, ids, context=None):
		'''
		This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
		'''
		assert len(ids) == 1, 'This option should only be used for a single id at a time'
		wf_service = netsvc.LocalService("workflow")
		wf_service.trg_validate(uid, 'sale.order', ids[0], 'quotation_sent', cr)
		datas = {
				 'model': 'sale.order',
				 'ids': ids,
				 'form': self.read(cr, uid, ids[0], context=context),
		}
		return {'type': 'ir.actions.report.xml', 'report_name': 'sale.quotation', 'datas': datas, 'nodestroy': True}

class sale_order_line(osv.osv):
	_inherit = "sale.order.line"
	_columns = {
		'pos': fields.char('Pos/Seq'),
	}