from osv import osv,fields

class res_partner(osv.osv):
	_inherit = 'res.partner'
	_columns = {
		'customer_number': fields.char('Customer Number')
	}

class res_partner_bank(osv.osv):
	_inherit = "res.partner.bank"
	_columns = {
		'bank_name': fields.char('Bank Name',size=100)
	}